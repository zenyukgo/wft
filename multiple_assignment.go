package main

var x int

func f() int {
  x = 3
  return x
}

func main() {
  x = 0
  a, _ := x, f()

  x = 0
  var b, _ = x, f()

  println(a, b)
}

// output: 3 0
