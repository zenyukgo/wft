package main

import (
  "fmt"
)

func main() {
  c := make(chan int, 5)
  c <- 5
  c <- 6
  close(c)
  fmt.Println(<-c)
}

// output: 5
